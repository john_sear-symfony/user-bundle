<?php declare(strict_types=1);

namespace JohnSear\JspUserBundle\DependencyInjection;

use Exception;
use JohnSear\JspUserBundle\DependencyInjection\UserManager\SystemUserManager;
use JohnSear\JspUserBundle\DependencyInjection\UserManager\UserManager;
use JohnSear\JspUserBundle\DependencyInjection\UserManager\UserProfileManager;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Exception\BadMethodCallException;
use Symfony\Component\DependencyInjection\Exception\InvalidArgumentException;

class JspUserExtension extends Extension
{
    /** @var ContainerBuilder */
    private $container;

    /**
     * @throws Exception
     */
    public function load(array $config, ContainerBuilder $container): void
    {
        $this->container = $container;

        $this->registerUserManager();
        $this->registerUserProfileManager();
        $this->registerSystemUserManager();

        $loader = new YamlFileLoader(
            $container,
            new FileLocator(__DIR__.'/../Resources/config')
        );
        $loader->load('services.yaml');
    }

    /**
     * @throws BadMethodCallException
     * @throws InvalidArgumentException
     */
    private function registerUserManager(): void
    {
        $definition = new Definition(UserManager::class, [
            new Reference('service_container'),
            new Reference('security.password_encoder'),
        ]);

        $this->container->setDefinition('jsp.user.user_manager', $definition)
            ->setPublic(true);
        $this->container->setAlias(UserManager::class, 'jsp.user.user_manager')
            ->setPublic(true);
    }

    /**
     * @throws BadMethodCallException
     * @throws InvalidArgumentException
     */
    private function registerUserProfileManager(): void
    {
        $definition = new Definition(UserProfileManager::class, [
            new Reference('service_container'),
            new Reference('security.password_encoder'),
        ]);

        $this->container->setDefinition('jsp.user.user_profile_manager', $definition)
            ->setPublic(true);
        $this->container->setAlias(UserProfileManager::class, 'jsp.user.user_profile_manager')
            ->setPublic(true);
    }

    /**
     * @throws BadMethodCallException
     * @throws InvalidArgumentException
     */
    private function registerSystemUserManager(): void
    {
        $definition = new Definition(SystemUserManager::class, [
            new Reference('jsp.user.user_manager'),
            new Reference('jsp.user.user_profile_manager'),
        ]);

        $this->container->setDefinition('jsp.user.system.user_manager', $definition)
            ->setPublic(true);
        $this->container->setAlias(SystemUserManager::class, 'jsp.user.system.user_manager')
            ->setPublic(true);
    }
}
