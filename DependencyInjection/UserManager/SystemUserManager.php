<?php declare(strict_types=1);

namespace JohnSear\JspUserBundle\DependencyInjection\UserManager;

use DateTime;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use JohnSear\JspUserBundle\Entity\User;
use JohnSear\JspUserBundle\Entity\UserProfile;
use JohnSear\Utilities\Uuid\Exception\InvalidUuidException;
use JohnSear\Utilities\Uuid\Exception\UuidGeneratorException;

class SystemUserManager
{
    public const SYSTEM_USER_UUID         = 'efe80892-407e-46bb-aa25-fbf099950c4c';
    public const SYSTEM_USER_PROFILE_UUID = 'c0027634-8400-41ac-b174-823b718ccc42';

    /** @var UserManager */
    private $userManager;
    /** @var UserProfileManager */
    private $userProfileManager;

    public function __construct(UserManager $userManager, UserProfileManager $userProfileManager)
    {
        $this->userManager = $userManager;
        $this->userProfileManager = $userProfileManager;
    }

    public function mayCreateSystemUser(): void
    {
        try {
            $systemUser = $this->getSystemUser();
            $systemUserProfile = $this->getSystemUserProfile();

            if (! $systemUser instanceof User || ! $systemUserProfile instanceof UserProfile) {
                if (!$systemUser instanceof User) {
                    $this->userManager->createUserByCredentials('system', '12345678', self::SYSTEM_USER_UUID, true);

                    $systemUser = $this->getSystemUser();
                }

                if (!($systemUserProfile instanceof UserProfile) && $systemUser instanceof User) {
                    $this->userProfileManager->createUserProfileByCredentials(
                        $systemUser,
                        'system@dawn.de',
                        'System',
                        'of a Dawn',
                        new DateTime('1970-01-01'),
                        $systemUser,
                        self::SYSTEM_USER_PROFILE_UUID,
                        true
                    );
                }
            }
        } catch (InvalidUuidException | ORMException | OptimisticLockException | UuidGeneratorException $ex) {
            // nothing for now
        }
    }

    public function getSystemUser(): ? User
    {
        return $this->userManager->getUser(self::SYSTEM_USER_UUID);
    }

    public function getSystemUserProfile(): ? UserProfile
    {
        return $this->userProfileManager->getUserProfile(self::SYSTEM_USER_PROFILE_UUID);
    }
}
