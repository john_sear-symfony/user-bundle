<?php declare(strict_types=1);

namespace JohnSear\JspUserBundle\DependencyInjection\UserManager;


use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use JohnSear\JspUserBundle\Entity\User;
use JohnSear\Utilities\Uuid\Exception\InvalidUuidException;
use JohnSear\Utilities\Uuid\Exception\UuidGeneratorException;

class UserManager extends AbstractUserManager
{
    /**
     * @throws InvalidUuidException
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws UuidGeneratorException
     */
    public function mayCreateUser(string $login, string $plainPassword, bool $doFlush = false): ? User
    {
        if (!$this->getUserByLogin($login) instanceof User) {
            $this->createUserByCredentials($login, $plainPassword, null, $doFlush);
        }

        return $this->getUserByLogin($login);
    }

    /**
     * @throws InvalidUuidException
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws UuidGeneratorException
     */
    public function createUserByCredentials(string $login, string $plainPassword, string $uuid = null, bool $doFlush = false): void
    {
        $user = new User($uuid);
        $user->setLogin($login);
        $user->setPassword($this->passwordEncoder->encodePassword($user, $plainPassword));

        $this->em->persist($user);

        if ($uuid !== null && $user->getId() !== $uuid) {
            $user->setId($uuid);
        }

        if($doFlush) {
            $this->em->flush($user);
        }
    }
}
