<?php declare(strict_types=1);

namespace JohnSear\JspUserBundle\DependencyInjection\UserManager;

use DateTime;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use JohnSear\JspUserBundle\Entity\User;
use JohnSear\JspUserBundle\Entity\UserProfile;
use JohnSear\Utilities\Uuid\Exception\InvalidUuidException;
use JohnSear\Utilities\Uuid\Exception\UuidGeneratorException;

class UserProfileManager extends AbstractUserManager
{
    /**
     * @throws InvalidUuidException
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws UuidGeneratorException
     */
    public function mayCreateUserProfile(User $user, bool $doFlush = false): ? UserProfile
    {
        if (!$this->getUserProfileByUser($user) instanceof UserProfile) {
            $creationUser = $this->getUser(SystemUserManager::SYSTEM_USER_UUID);
            $this->createUserProfileFromUser($user, null, $creationUser, $doFlush);
        }

        return $this->getUserProfileByUser($user);
    }

    /**
     * @throws InvalidUuidException
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws UuidGeneratorException
     */
    public function createUserProfileFromUser(User $user, string $uuid = null, User $creationUser = null, bool $doFlush = false): void
    {
        $creationUser = ($creationUser instanceof User) ? $creationUser : $this->getUser(SystemUserManager::SYSTEM_USER_UUID);
        $this->createUserProfileByCredentials($user, null, null, null, null, $creationUser, $uuid, $doFlush);
    }

    /**
     * @throws InvalidUuidException
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws UuidGeneratorException
     */
    public function createUserProfileByCredentials(
        User $user,
        string $email         = null,
        string $firstName     = null,
        string $lastName      = null,
        DateTime $dateOfBirth = null,
        User $creationUser    = null,
        string $uuid          = null,

        bool $doFlush = false
    ): void
    {
        $firstName    = (is_string($firstName)) ? $firstName : '';
        $lastName     = (is_string($lastName)) ? $lastName : '';
        $email        = (is_string($email) && filter_var($email, FILTER_VALIDATE_EMAIL)) ? $email : $user->getLogin() . '@project.com';
        $dateOfBirth  = ($dateOfBirth instanceof DateTime) ? $dateOfBirth : new DateTime('1970-01-01');
        $creationUser = ($creationUser instanceof User) ? $creationUser : $this->getUser(SystemUserManager::SYSTEM_USER_UUID);

        $userProfile = (new UserProfile($uuid))
            ->setUser($user)
            ->setEmail($email)
            ->setFirstName($firstName)
            ->setLastName($lastName)
            ->setDateOfBirth($dateOfBirth)
        ;

        $userProfile->setCurrentUser($creationUser);

        $this->em->persist($userProfile);

        if ($uuid !== null && $userProfile->getId() !== $uuid) {
            $userProfile->setId($uuid);
        }

        if($doFlush) {
            $this->em->flush($userProfile);
        }
    }
}
