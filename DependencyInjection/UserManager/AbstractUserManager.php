<?php declare(strict_types=1);

namespace JohnSear\JspUserBundle\DependencyInjection\UserManager;

use Doctrine\ORM\EntityManager;
use JohnSear\JspUserBundle\Entity\User;
use JohnSear\JspUserBundle\Entity\UserProfile;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

abstract class AbstractUserManager
{
    /** @var EntityManager */
    protected $em;
    /** @var UserPasswordEncoderInterface */
    protected $passwordEncoder;

    public function __construct(ContainerInterface $container, UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->em = $container->get('doctrine.orm.entity_manager');
        $this->passwordEncoder = $passwordEncoder;
    }

    public function getUser(string $uuid): ? User
    {
        $userRepository = $this->em->getRepository(User::class);

        return $userRepository->find($uuid);
    }

    public function getUserByLogin(string $login): ? User
    {
        $userRepository = $this->em->getRepository(User::class);

        return $userRepository->findOneBy(['login' => $login]);
    }

    public function getUserProfile(string $uuid): ? UserProfile
    {
        $userProfileRepository = $this->em->getRepository(UserProfile::class);

        return $userProfileRepository->find($uuid);
    }

    public function getUserProfileByUser(User $user): ? UserProfile
    {
        $userProfileRepository = $this->em->getRepository(UserProfile::class);

        return $userProfileRepository->findOneBy(['user' => $user]);
    }

    public static function createSalt($length = 64): string
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        return bin2hex(random_bytes($length));
    }
}
