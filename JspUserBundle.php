<?php declare(strict_types=1);

namespace JohnSear\JspUserBundle;

use JohnSear\JspUserBundle\DependencyInjection\JspUserExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class JspUserBundle extends Bundle
{
    public function getContainerExtension()
    {
        return new JspUserExtension();
    }
}
