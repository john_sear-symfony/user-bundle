# User Bundle
(c) 2019 by [John_Sear](https://bitbucket.org/john_sear-symfony/user-bundle/)

## Symfony Bundle

This is a Symfony Bundle to use User with User Profile in your symfony application.  
Be sure you are using symfony version 4.4.

> This Bundle is still in Development. Some things can be broken ;-)

## Installation

### via CLI
Run ``composer require jsp/user-bundle`` command in cli to install source with Version 0.1

### composer.json
Add following to your symfony application composer json file:
```json
{
  "require": {
    "jsp/user-bundle": "*"
  }
}
```

## Documentation
Documentation can be found [here](Resources/doc/index.md)
