<?php declare(strict_types=1);

namespace JohnSear\JspUserBundle\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JohnSear\JspUserBundle\DependencyInjection\UserManager\UserManager;
use JohnSear\Utilities\Uuid\Entity\AbstractUuidEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="JohnSear\JspUserBundle\Repository\UserRepository")
 * @ORM\Table(name="jsp_user")
 * @UniqueEntity(fields="login", message="jsp.user.login.unique")
 */
class User extends AbstractUuidEntity implements UserInterface
{
    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $login;

    /**
     * @ORM\Column(type="array")
     */
    private $roles;

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @var string The user salt
     * @ORM\Column(type="text")
     */
    private $salt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lastLogin;

    public function __construct(string $uuid = null)
    {
        parent::__construct($uuid);
        $this->roles = new ArrayCollection();
        $this->salt = UserManager::createSalt();
    }

    protected function getDisplayName(): string
    {
        return $this->getUsername();
    }

    public function getLogin(): string
    {
        return (string) $this->login;
    }

    public function setLogin(string $login): self
    {
        $this->login = $login;

        return $this;
    }

    /**
     * Returns the username used to authenticate the user.
     *
     * @return string The username
     */
    public function getUsername(): string
    {
        return $this->getLogin();
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->getRolesAsCollection();

        return $roles->toArray();
    }

    public function setRoles(array $roles): self
    {
        foreach ($roles as $role) {
            $this->addRole($role);
        }

        return $this;
    }

    public function addRole(string $role): self
    {
        $this->roles->add($role);

        return $this;
    }

    public function removeRole(string $role): self
    {
        $this->roles->removeElement($role);

        return $this;
    }

    public function getRolesAsCollection(): Collection
    {
        $roles = $this->roles;

        $this->guaranteeRolesContainsRoleUser($roles);
        $this->guaranteeRolesAreUnique($roles);

        return $roles;
    }

    private function guaranteeRolesContainsRoleUser(Collection &$roles): void
    {
        $roles->add('ROLE_USER');
    }

    private function guaranteeRolesAreUnique(Collection &$roles): void
    {
        $uniqueRoles = new ArrayCollection();

        $uniqueRolesArray = array_unique($roles->toArray());
        foreach ($uniqueRolesArray as $role) {
            $uniqueRoles->add($role);
        }

        $roles = $uniqueRoles;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getLastLogin(): ? DateTime
    {
        return $this->lastLogin;
    }

    public function setLastLogin(DateTime $lastLogin): self
    {
        $this->lastLogin = $lastLogin;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt(): string
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
        return $this->salt;
    }

    public function setSalt(string $salt): self
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }
}
