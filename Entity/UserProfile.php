<?php declare(strict_types=1);

namespace JohnSear\JspUserBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use JohnSear\JspLoggingEntityBundle\Logging\AbstractCreationAndUpdateLoggingEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="JohnSear\JspUserBundle\Repository\UserProfileRepository")
 * @ORM\Table(name="jsp_user_profile")
 * @UniqueEntity(fields="email", message="jsp.user.profile.email.unique")
 * @ORM\HasLifecycleCallbacks()
 */
class UserProfile extends AbstractCreationAndUpdateLoggingEntity
{
    /**
     * @ORM\OneToOne(targetEntity="JohnSear\JspUserBundle\Entity\User")
     */
    private $user;

    /**
     * @ORM\Column(type="string")
     */
    private $firstName;

    /**
     * @ORM\Column(type="string")
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateOfBirth;

    public function getUser(): ? User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getFirstName(): string
    {
        return (string) $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): string
    {
        return (string) $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getFullName(): string
    {
        if(
            ($this->getFirstName() !== '' || $this->getLastName() !== '')
        ) {
            return $this->getFirstName() . ' ' . $this->getLastName();
        }

        if ($this->getUser() instanceof User) {
            return $this->getUser()->getLogin();
        }

        return $this->getEmail();
    }

    public function getEmail(): string
    {
        return (string) $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getDateOfBirth(): ? DateTime
    {
        return $this->dateOfBirth;
    }

    public function setDateOfBirth(DateTime $date): self
    {
        $this->dateOfBirth = $date;

        return $this;
    }
}
