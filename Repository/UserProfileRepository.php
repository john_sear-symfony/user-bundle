<?php declare(strict_types=1);

namespace JohnSear\JspUserBundle\Repository;

use JohnSear\JspLoggingEntityBundle\Logging\AbstractCreationAndUpdateLoggingRepository;
use JohnSear\JspUserBundle\Entity\User;
use JohnSear\JspUserBundle\Entity\UserProfile;

/**
 * @method UserProfile|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserProfile|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserProfile[]    findAll()
 * @method UserProfile[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserProfileRepository extends AbstractCreationAndUpdateLoggingRepository
{
    public function getEntityClass(): string
    {
        return UserProfile::class;
    }

    public function findOneByUser(User $user): ? UserProfile
    {
        return $this->findOneBy(['user' => $user->getId()]);
    }
}
