<?php declare(strict_types=1);

namespace JohnSear\JspUserBundle\Exception;

class NoUserFoundException extends \Exception
{
    
}
