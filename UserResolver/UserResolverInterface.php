<?php declare(strict_types=1);

namespace JohnSear\JspUserBundle\UserResolver;

use JohnSear\JspUserBundle\Exception\NotAuthenticatedException;
use JohnSear\JspUserBundle\Exception\NoValidUserAuthenticatedException;
use JohnSear\JspUserBundle\Entity\User;

interface UserResolverInterface
{
    /**
     * @throws NoValidUserAuthenticatedException
     * @throws NotAuthenticatedException
     */
    public function getCurrentUser(): User;

    public function hasRole(string $role): bool;
}
