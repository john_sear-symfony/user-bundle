<?php declare(strict_types=1);

namespace JohnSear\JspUserBundle\UserResolver;

use Doctrine\Common\Collections\Collection;
use JohnSear\JspUserBundle\Exception\NotAuthenticatedException;
use JohnSear\JspUserBundle\Exception\NoValidUserAuthenticatedException;
use JohnSear\JspUserBundle\Entity\User;
use Psr\Container\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class UserResolver implements UserResolverInterface
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @throws NoValidUserAuthenticatedException
     * @throws NotAuthenticatedException
     */
    public function getCurrentUser(): User
    {
        /** @var TokenInterface $token */
        $token = $this->container->get('security.token_storage')->getToken();

        if ($token === null) {
            throw new NotAuthenticatedException('Not authenticated');
        }

        $user = $token->getUser();

        if (! $user instanceof User) {
            throw new NoValidUserAuthenticatedException('No Valid User authenticated');
        }

        return $user;
    }

    public function hasRole(string $role): bool
    {
        try {
            $currentRoles = $this->getCurrentRoles();
            $hasRole = $currentRoles->contains($role);
        } catch (NoValidUserAuthenticatedException | NotAuthenticatedException $ex) {
            $hasRole = false;
        }

        return $hasRole;
    }

    /**
     * @throws NoValidUserAuthenticatedException
     * @throws NotAuthenticatedException
     */
    private function getCurrentRoles(): Collection
    {
        $user = $this->getCurrentUser();

        return $user->getRolesAsCollection();
    }
}
