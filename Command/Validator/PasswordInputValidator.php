<?php declare(strict_types=1);

namespace JohnSear\JspUserBundle\Command\Validator;

use JohnSear\JspUserBundle\DependencyInjection\UserManager\UserManager;
use JohnSear\JspUserBundle\Entity\User;
use JohnSear\Utilities\Command\InputValidatorInterface;
use Symfony\Component\Console\Exception\InvalidArgumentException;

class PasswordInputValidator implements InputValidatorInterface
{
    /** @var UserManager */
    private $userManager;

    public function __construct(UserManager $userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * @throws InvalidArgumentException
     */
    public function validateUserInput(string $UserInput): void
    {
        $userLogin = $UserInput;

        if ($userLogin === '') {
            throw new InvalidArgumentException('User login cannot be empty!');
        }
        if ($userLogin !== strtolower($userLogin)) {
            throw new InvalidArgumentException('User login should not contain uppercase Letters!');
        }
        if (strpos($userLogin, ' ') !== false) {
            throw new InvalidArgumentException('User login should not contain whitespace!');
        }
        if ($this->userManager->getUserByLogin($userLogin) instanceof User) {
            throw new InvalidArgumentException('User with login already exists!');
        }
    }
}
