<?php declare(strict_types=1);

namespace JohnSear\JspUserBundle\Command\Validator;

use JohnSear\JspUserBundle\DependencyInjection\UserManager\UserManager;
use JohnSear\Utilities\Command\CommandValidatorInterface;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreateUserValidator implements CommandValidatorInterface
{
    public const CREATE_OPTION_SYSTEM_USER  = 'system-user';
    public const CREATE_OPTION_USER_LOGIN   = 'login';
    public const CREATE_OPTION_USER_PASS    = 'password';
    public const CREATE_OPTION_USER_PROFILE = 'user-profile';

    /** @var PasswordInputValidator */
    private $passwordInputValidator;


    public function __construct(UserManager $userManager)
    {
        $this->passwordInputValidator = new PasswordInputValidator($userManager);
    }

    /**
     * @throws InvalidArgumentException
     */
    public function getFlag(InputInterface $input): string
    {
        $createOption = '';

        $optionSystemUser = $input->getOption(self::CREATE_OPTION_SYSTEM_USER);
        $optionUserProfile = $input->getOption(self::CREATE_OPTION_USER_PROFILE);

        if ($optionSystemUser){
            $createOption = self::CREATE_OPTION_SYSTEM_USER;
        } elseif ($optionUserProfile) {
            $createOption = self::CREATE_OPTION_USER_PROFILE;
        }

        return $createOption;
    }

    /**
     * @throws InvalidArgumentException
     */
    public function validate(InputInterface $input, OutputInterface $output, bool $verbose = true): bool
    {
        $errorMessage = '';

        $updateOption = $this->getFlag($input);

        if (!$updateOption !== self::CREATE_OPTION_SYSTEM_USER) {
            $optionLogin = (string)$input->getOption(self::CREATE_OPTION_USER_LOGIN);
            $optionPass = (string)$input->getOption(self::CREATE_OPTION_USER_PASS);

            if (($optionLogin === '' || $optionPass === '') && !$input->isInteractive()) {

                $login = '';
                if ($optionLogin === '') {
                    $login = 'Login';
                }

                $password = '';
                if ($optionPass === '') {
                    $and = ($login !== '') ? ' and ' : '';
                    $password = $and . 'Password';
                }

                $errorMessage = 'When using "no interaction", please add necessary <comment>' . $login . $password . '</comment> string to create an User.';
            }
        }

        if($errorMessage !== '') {
            $output->writeln([
                '',
                '<fg=red>User Creation Failure:<fg=default> ' . $errorMessage,
                '',
                'Add <info>--help</info> for usage information!',
                '',
            ]);

            return false;
        }

        return true;
    }

    public function getPasswordInputValidator(): PasswordInputValidator
    {
        return $this->passwordInputValidator;
    }
}
