<?php declare(strict_types=1);

namespace JohnSear\JspUserBundle\Command;

use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use JohnSear\JspUserBundle\Command\Validator\CreateUserValidator;
use JohnSear\JspUserBundle\DependencyInjection\UserManager\SystemUserManager;
use JohnSear\JspUserBundle\DependencyInjection\UserManager\UserManager;
use JohnSear\JspUserBundle\DependencyInjection\UserManager\UserProfileManager;
use JohnSear\JspUserBundle\Entity\User;
use JohnSear\JspUserBundle\Entity\UserProfile;
use JohnSear\Utilities\Command\AbstractCommand;
use JohnSear\Utilities\Command\CommandConfiguration;
use JohnSear\Utilities\Uuid\Exception\InvalidUuidException;
use JohnSear\Utilities\Uuid\Exception\UuidGeneratorException;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Logger\ConsoleLogger;
use Symfony\Component\Console\Output\OutputInterface;

class CreateUserCommand extends AbstractCommand
{
    /** @var SystemUserManager */
    private $systemUserManager;
    /** @var UserManager */
    private $userManager;
    /** @var UserProfileManager */
    private $userProfileManager;

    /** @var CreateUserValidator */
    private $validator;

    public function __construct(SystemUserManager $systemUserManager, UserManager $userManager, UserProfileManager $userProfileManager)
    {
        $this->systemUserManager  = $systemUserManager;
        $this->userManager        = $userManager;
        $this->userProfileManager = $userProfileManager;
        $this->validator          = new CreateUserValidator($userManager);

        parent::__construct($this->createConfiguration());
    }

    private function createConfiguration(): CommandConfiguration
    {
        $commandConfiguration = (new CommandConfiguration())
            ->setName('jsp:user:create')
            ->setDisplayName('<fg=blue>JohnSear JspUserBundle<fg=default> - User Creator')
            ->setDescription('Create a System User or an User with or without UserProfile.');

        $commandConfiguration->addOption(
            CreateUserValidator::CREATE_OPTION_SYSTEM_USER,
            null,
            InputOption::VALUE_NONE,
            'Create a System User, if not exists'
        );

        $commandConfiguration->addOption(
            CreateUserValidator::CREATE_OPTION_USER_LOGIN,
            null,
            InputOption::VALUE_OPTIONAL,
            'Login, first part from user credentials'
        );

        $commandConfiguration->addOption(
            CreateUserValidator::CREATE_OPTION_USER_PASS,
            null,
            InputOption::VALUE_OPTIONAL,
            'Password, second part from user credentials'
        );

        $commandConfiguration->addOption(
            CreateUserValidator::CREATE_OPTION_USER_PROFILE,
            null,
            InputOption::VALUE_NONE,
            'Create User with additional UserProfile'
        );

        $commandConfiguration->setCommandValidator($this->validator);

        return $commandConfiguration;
    }

    /**
     * @throws InvalidArgumentException
     */
    protected function executeActions(InputInterface $input, OutputInterface $output): void
    {
        $output->writeln('');

        switch ($this->validator->getFlag($input)) {
            case CreateUserValidator::CREATE_OPTION_SYSTEM_USER:
                $output->write('# Creating <info>System User</info>');
                $this->createSystemUser($output);
                break;
            case CreateUserValidator::CREATE_OPTION_USER_PROFILE:
                $output->write('# Creating <info>new User</info> with <info>UserProfile</info>');
                $this->createUser($input, $output, true);
                break;
            default:
                $output->write('# Creating <info>new User</info>');
                $this->createUser($input, $output);
                break;
        }
    }

    private function createSystemUser(OutputInterface $output): void
    {
        $output->writeln('');

        if ($this->systemUserManager->getSystemUser() instanceof User) {
            $output->writeln('  - System User already created. <info>-> Creation skipped!</info>');
            $output->writeln('');

            return;
        }

        $output->writeln('  - Creating System User');

        $this->systemUserManager->mayCreateSystemUser();

        if ($this->systemUserManager->getSystemUser() instanceof User) {
            $output->writeln('    > <info>System User created</info>');
        } else {
            $output->write('    > <fg=red>System User creation failed...<fg=default></>');
        }

        $output->writeln('');
    }

    /**
     * @throws InvalidArgumentException
     */
    private function createUser(InputInterface $input, OutputInterface $output, bool $withUserProfile = false): void
    {
        $output->writeln('');

        if ($input->isInteractive()) {
            $userLogin = $this->askForLogin($input, $output);
            if ($userLogin === '') {
                return;
            }
            $output->writeln('');

            $userPassword = $this->askForPassword($input, $output);
            if ($userPassword === '') {
                return;
            }
        } else {
            $userLogin    = (string) $input->getOption(CreateUserValidator::CREATE_OPTION_USER_LOGIN);
            $userPassword = (string) $input->getOption(CreateUserValidator::CREATE_OPTION_USER_PASS);
        }

        $foundUser = $this->userManager->getUserByLogin($userLogin);

        if(!$foundUser instanceof User) {
            $output->writeln('');
            $output->writeln('  - Creating new User');

            $newUser = $this->createNewUserByUserInput($userLogin, $userPassword);

            if (!$newUser instanceof User) {
                $this->writeError($output, 'Could not create User');

                return;
            }

            $output->writeln('    > New User with Uuid "<comment>' . $newUser->getId() . '</comment>" created!');

            $user = $newUser;
        } else {
            $user = $foundUser;
            $output->writeln('  - User with Login "<comment>' . $foundUser->getLogin() . '</comment>" found. <info>-> Creation skipped!</info>');
        }

        if (!$user instanceof User) {
            $this->writeError($output, 'User creation failed...');

            return;
        }

        if (! $withUserProfile) {
            $output->writeln('');

            return;
        }

        $foundUserProfile = $this->userProfileManager->getUserProfileByUser($user);

        if ($foundUserProfile instanceof UserProfile) {
            $output->writeln('  - UserProfile from User with Login "<comment>' . $user->getLogin() . '</comment>" found. <info>-> Creation skipped!</info>');

            $output->writeln('');

            return;
        }

        $output->writeln('  - Creating new User Profile');

        try {
            $this->userProfileManager->mayCreateUserProfile($user, true);
        } catch (OptimisticLockException | ORMException | InvalidUuidException | UuidGeneratorException $ex) {
            $this->writeError($output, 'Could not create UserProfile: <fg=default>' . $ex->getMessage());

            return;
        }

        $newUserProfile = $this->userProfileManager->getUserProfileByUser($user);

        if (!$newUserProfile instanceof UserProfile) {
            $this->writeError($output, 'Could not create UserProfile');

            return;
        }
        $output->writeln('    > New UserProfile with Uuid "<comment>' . $newUserProfile->getId() . '</comment>" created!');

        $output->writeln('');
    }

    private function createNewUserByUserInput(string $userLogin, string $userPassword): ? User
    {
        try {
            $this->userManager->mayCreateUser($userLogin, $userPassword, true);
        } catch (OptimisticLockException | ORMException | InvalidUuidException | UuidGeneratorException $ex) {
            $this->log(ConsoleLogger::ERROR, 'Could not create User: <fg=default>' . $ex->getMessage());

            return null;
        }

        return $this->userManager->getUserByLogin($userLogin);
    }

    public function askForLogin(InputInterface $input, OutputInterface $output): string
    {
        $help       = 'Please enter the <comment>login</comment> for the new User (Please use lowercase and no whitespace!)';
        $question   = 'User login';
        $retryCount = 3;

        $optionLogin = (string) $input->getOption(CreateUserValidator::CREATE_OPTION_USER_LOGIN);
        $default = ($optionLogin !== '' && $input->isInteractive()) ? $optionLogin : '';

        $userLogin = $this->askFor($input, $output, $question, $default, $help, $retryCount, $this->validator->getPasswordInputValidator());

        if ($userLogin === '') {
            $this->writeError($output, 'Login not valid...');

            return $userLogin;
        }

        $output->writeln('  - Thank you, you have set <info>"' . $userLogin . '"</info> as login :-)');

        return $userLogin;
    }

    public function askForPassword(InputInterface $input, OutputInterface $output): string
    {
        $help       = 'Please enter the <comment>password</comment> of the new User';
        $question   = 'User password';
        $retryCount = 3;

        $optionPass  = (string) $input->getOption(CreateUserValidator::CREATE_OPTION_USER_PASS);
        $default = ($optionPass !== '' && $input->isInteractive()) ? $optionPass : '';

        $userPassword = $this->askFor($input, $output, $question, $default, $help, $retryCount);

        if ($userPassword === '') {
            $this->writeError($output, 'Password not valid...');

            return $userPassword;
        }

        $output->writeln('  - Thank you, you have set the password ;-)');

        return $userPassword;
    }
}
