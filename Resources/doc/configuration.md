# Symfony Configuration

### Security
Add these information to your security.yaml file

```yaml
security:
    encoders:
        JohnSear\JspUserBundle\Entity\User:
            algorithm: auto

    # https://symfony.com/doc/current/security.html#where-do-users-come-from-user-providers
    providers:
        # used to reload user from session & other features (e.g. switch_user)
        app_user_provider:
            entity:
                class: JohnSear\JspUserBundle\Entity\User
                property: login

# ..
```

### Register all Services

For now, all services must be auto wired via the services.yaml.

Add following Lines at the end of ``config\services.yaml``

```yaml
services:

    # ..

    # add more service definitions when explicit configuration is needed
    # please note that last definitions always *replace* previous ones

    JohnSear\JspUserBundle\:
        resource: '../vendor/jsp/user-bundle/*'
# ..
```
