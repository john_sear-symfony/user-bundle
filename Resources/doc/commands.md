# Commands
## User Creation `jsp:user:create`

> Important: First create SystemUser before creating other users

### SystemUser creation
For several actions, the System user is needed.

#### Usage

```bash
php bin/console jsp:user:create --system-user
```

#### Result

```bash
==> JohnSear JspUserBundle - User Creator <==

# Creating System User
  - Creating System User
    > System User created

...all done!
```

### User Creation with interaction
Just use the interactive way to create a new user.  
With option `--user-profile` an additional User Profile will be created.

If an User or an User Profile already exists, the creation Process will be skipped.

> The User Profile will be created only with basic information.  
> They can be added after creation.

#### Usage

```bash
php bin/console jsp:user:create --user-profile
```

##### Step 1
Set the "login" for the new user

```bash
==> JohnSear JspUserBundle - User Creator <==

# Creating new User with UserProfile
Please enter the login for the new User (Please use lowercase and no whitespace!)
User login: john_doe
  - Thank you, you have set "john_doe" as login :-)
```

##### Step 2
Set the "password" for the new user

```bash
==> JohnSear JspUserBundle - User Creator <==

# Creating new User with UserProfile
Please enter the login for the new User (Please use lowercase and no whitespace!)
User login: john_doe
  - Thank you, you have set "john_doe" as login :-)

Please enter the password of the new User
User password: 12345678
  - Thank you, you have set the password ;-)
```

#### Result

```bash
==> JohnSear JspUserBundle - User Creator <==

# Creating new User with UserProfile
Please enter the login for the new User (Please use lowercase and no whitespace!)
User login: john_doe
  - Thank you, you have set "john_doe" as login :-)

Please enter the password of the new User
User password: 12345678
  - Thank you, you have set the password ;-)

  - Creating new User
    > New User with Uuid "27531de3-7c60-4a2a-97f8-21473e86f521" created!
  - Creating new User Profile
    > New UserProfile with Uuid "d911f5f8-d116-4904-84ee-e243c22ad4c8" created!

...all done!
```

### User Creation without interaction
User creation can be done without interaction. Just add `--no-interaction` option to the command.
Required options `--login=""` and `--password=""` must be set.

If an User or an User Profile already exists, the creation Process will be skipped.

#### Usage
```bash
php bin\console jsp:user:create --login="john_doe" --password="12345678" --user-profile --no-interaction
```

#### Result 

```bash
==> JohnSear JspUserBundle - User Creator <==

# Creating new User with UserProfile

  - Creating new User
    > New User with Uuid "27531de3-7c60-4a2a-97f8-21473e86f521" created!
  - Creating new User Profile
    > New UserProfile with Uuid "d911f5f8-d116-4904-84ee-e243c22ad4c8" created!

...all done!
```

